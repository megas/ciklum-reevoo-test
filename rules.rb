require "./core_ext"

class PatternFixedPriceRule
  
  def initialize pattern, pattern_price
    @pattern, @pattern_price = pattern, pattern_price
  end

  def total_for items
    temp_items, counter = items.dup, 0
    
    while (ind = temp_items.locate_pattern @pattern)
      temp_items.fill(nil, ind, @pattern.length)
      counter += 1
    end
    
    [counter * @pattern_price, temp_items]
  end
end

class ItemDiscountPriceRule
  
  def initialize item, quantity, discounted_price
    @item, @quantity, @discounted_price = item, quantity, discounted_price
  end

  def total_for items
    temp_items, counter = items.dup, items.count(@item)
    
    if counter >= @quantity
      temp_items.map! { |el| el == @item ? nil : el }
      [counter * @discounted_price, temp_items]
    else
      [0, temp_items]
    end
  
  end
end