require 'bundler/setup'
require "rspec"
require './rules'

describe "Rules" do
  context "Simple rules for pattern fixed price" do
    before(:each) { @rule = PatternFixedPriceRule.new(["CF1"], 11.23) }

    it "should accept return zero for empty cart" do
      @rule.total_for([]).should == [0, []]
    end

    it "should return correct price for single item" do
      @rule.total_for(["CF1"]).should == [11.23, [nil]]
    end

    it "should return correct price for multi same items" do
      @rule.total_for(["CF1", "CF1"]).should == [11.23*2, [nil, nil]]
    end

    it "should ignore items not of rule type" do
      @rule.total_for(["CF1", "CF1", "SR1"]).should == [11.23*2, [nil, nil, "SR1"]]
    end
  end

  context "Complex rules for pattern single price" do
    before(:each) { @rule = PatternFixedPriceRule.new(["FR1", "FR1"], 3.11) }

    it "should return correct sum for right group order" do
      @rule.total_for(["FR1", "FR1", "CF1"]).should == [3.11, [nil, nil, "CF1"]]
    end

    it "should return zero for incorrect group order" do
      @rule.total_for(["FR1", "CF1", "FR1"]).should == [0, ["FR1", "CF1", "FR1"]]
    end
  end

  context "Rules for items discount" do
    before(:each) { @rule = ItemDiscountPriceRule.new("SR1", 3, 4.5)}

    it "should give discount for more than needed items" do
      @rule.total_for(["FR1", "SR1", "SR1", "SR1"]).should == [13.5, ["FR1", nil, nil, nil]]
    end

    it "should not give discount for less then needed items" do
      @rule.total_for(["FR1", "SR1", "SR1"]).should == [0, ["FR1", "SR1", "SR1"]]
    end
  end
end