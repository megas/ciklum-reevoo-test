require 'bundler/setup'
require "rspec"
require './checkout'
require './rules'

PRICING_RULES = [
                PatternFixedPriceRule.new(["FR1", "FR1"], 3.11),
                ItemDiscountPriceRule.new("SR1", 3, 4.5),
                PatternFixedPriceRule.new(["FR1"], 3.11),
                PatternFixedPriceRule.new(["SR1"], 5.0),
                PatternFixedPriceRule.new(["CF1"], 11.23)
                ]
describe "Checkout" do

  it "should return zero for empty cart" do
    co = Checkout.new(PRICING_RULES)
    co.total.should == 0
  end

  context "Simple tests" do
    before(:each) do
      @co = Checkout.new(PRICING_RULES)
    end

    it "should return correct total for fruit tea" do
      @co.scan "FR1"
      @co.total.should == 3.11
    end

    it "should return correct total for fruit tea and coffee" do
      @co.scan "FR1"
      @co.scan "CF1"
      @co.total.should == 3.11+11.23
    end

  end

  context "Target test cases" do
    before(:each) do
      @co = Checkout.new(PRICING_RULES)
    end

    it "should return 22.45 for FR1, SR1, FR1, CF1" do
      %w(FR1 SR1 FR1 CF1).each do |item|
        @co.scan item
      end
      @co.total.should == 3.11 + 5.00 + 3.11 + 11.23
    end

    it "should return 3.11 for FR1, FR1" do
      %w(FR1 FR1).each do |item|
        @co.scan item
      end
      @co.total.should == 3.11
    end

    it "should return 16.61 for SR1, SR1, FR1, SR1" do
      %w(SR1 SR1 FR1 SR1).each do |item|
        @co.scan item
      end
      @co.total.should == 16.61
    end
  end
end