class Checkout

  def initialize pricing_rules
    @items = []
    @rules = pricing_rules
  end

  def total
    all_subtotal = 0
    left_items = @items.dup
    @rules.each do |rule|
      subtotal, left_items   = rule.total_for(left_items)
      all_subtotal += subtotal
    end
    all_subtotal
  end

  def scan item_sku
    @items << item_sku
  end
end