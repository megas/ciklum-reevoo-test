class Array
  def locate_pattern needle
    self.each_index do |ind|
      return ind if self[ind, needle.length] == needle
    end
    nil
  end
end